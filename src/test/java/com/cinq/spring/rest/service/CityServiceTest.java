package com.cinq.spring.rest.service;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.cinq.spring.rest.model.City;

@SpringBootTest
public class CityServiceTest {

	@Autowired
	CityService cityService;

	@Test
	public void testFindCitiesByCountryWithParameter() {
		List<City> result = cityService.findCitiesByCountryName("br");
		Assertions.assertTrue(result.size() > 0);
	}

	@Test
	public void testFindCitiesByCountryWithNotValidParameter() {
		List<City> result = cityService.findCitiesByCountryName("BBBBBB");
		Assertions.assertTrue(result != null);
		Assertions.assertTrue(result.size() == 0);
	}

	@Test
	public void testFindCitiesByCountryWithEmptyParameter() {
		List<City> result = cityService.findCitiesByCountryName("");
		Assertions.assertTrue(result != null);
		Assertions.assertTrue(result.size() == 0);
	}
	@Test
	public void testFindCitiesByCountryWithNullParameter() {
		List<City> result = cityService.findCitiesByCountryName(null);
		Assertions.assertTrue(result != null);
		Assertions.assertTrue(result.size() == 0);
	}
	

}
