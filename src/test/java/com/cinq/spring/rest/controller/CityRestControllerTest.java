package com.cinq.spring.rest.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class CityRestControllerTest {

    @LocalServerPort
    int randomServerPort;
    
    @Test
    public void testFindCitiesByCountryNameSuccess() throws URISyntaxException 
    {
        RestTemplate restTemplate = new RestTemplate();
         
        final String baseUrl = "http://localhost:" + randomServerPort + "/rest/cities?country=br";
        URI uri = new URI(baseUrl);
     
        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
         
        //Verify request succeed
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }

    
    @Test
    public void testFindCitiesByCountryNameEmpty() throws URISyntaxException 
    {
        RestTemplate restTemplate = new RestTemplate();
         
        final String baseUrl = "http://localhost:" + randomServerPort + "/rest/cities?country=";
        URI uri = new URI(baseUrl);
     
        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
         
        //Verify request succeed
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void testFindCitiesByCountryNotFound() throws URISyntaxException 
    {
        RestTemplate restTemplate = new RestTemplate();
         
        final String baseUrl = "http://localhost:" + randomServerPort + "/rest/cities?country=bbbbb";
        URI uri = new URI(baseUrl);
     
        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
         
        //Verify request succeed
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }
}
