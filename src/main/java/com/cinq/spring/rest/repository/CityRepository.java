package com.cinq.spring.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cinq.spring.rest.model.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long>{

	List<City> findCityByCountryNameStartsWithIgnoreCase(String country);
}
