package com.cinq.spring.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cinq.spring.rest.model.City;
import com.cinq.spring.rest.model.Country;
import com.cinq.spring.rest.repository.CityRepository;
import com.cinq.spring.rest.repository.CountryRepository;

@SpringBootApplication
public class SpringRestApplication implements CommandLineRunner {
	
	private static final Logger log = LoggerFactory.getLogger(SpringRestApplication.class);
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(SpringRestApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);	}

	@Override
	public void run(String... args) throws Exception {
		
	    log.info("Clean up tables..");
		cityRepository.deleteAll();
		countryRepository.deleteAll(); 	
		
		Country brazil = new Country(1, "Brazil");
		Country usa = new Country(2, "United States");
		Country france = new Country(3, "France");

	    log.info("Add Countries...");
		countryRepository.save(brazil);
		countryRepository.save(usa);
		countryRepository.save(france);
	
	    log.info("Add Cities...");
		cityRepository.save(new City("Curitiba", brazil));
		cityRepository.save(new City("Rio de Janeiro", brazil));
		cityRepository.save(new City("Manaus", brazil));
		cityRepository.save(new City("Fortaleza", brazil));

		cityRepository.save(new City("New York", usa));
		cityRepository.save(new City("Los Angeles", usa));
		cityRepository.save(new City("Atlanta", usa));

		cityRepository.save(new City("Paris", france));
		cityRepository.save(new City("Lyon", france));
		
	    log.info("FindAllCities()..");
	    cityRepository.findAll().forEach(p -> log.info(p.toString()));
	}

}
