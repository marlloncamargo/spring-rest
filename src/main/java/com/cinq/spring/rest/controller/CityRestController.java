package com.cinq.spring.rest.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cinq.spring.rest.model.City;
import com.cinq.spring.rest.service.CityService;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CityRestController {

	@Autowired
	private CityService service;;
	
	@GetMapping("/cities")
	public List<City> findCitiesByCountryName(@PathParam("country") String country) {
		return service.findCitiesByCountryName(country);
	}
	
}
