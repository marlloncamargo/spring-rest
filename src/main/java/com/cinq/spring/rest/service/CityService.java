package com.cinq.spring.rest.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cinq.spring.rest.model.City;
import com.cinq.spring.rest.repository.CityRepository;

@Service
public class CityService {
	
	@Autowired
	private CityRepository repository;
	
	public List<City> findCitiesByCountryName(String country) {
		if (country == null || country.isEmpty()) return new ArrayList<City>();
		return repository.findCityByCountryNameStartsWithIgnoreCase(country);
	}
}
